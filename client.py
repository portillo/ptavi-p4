#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente UDP que abre un socket a un servidor
"""

import socket
import sys

# Constantes. Dirección IP del servidor y contenido a enviar
if len(sys.argv) != 6:
    print('Usage: client.py ip puerto register sip_address expires_value')
    sys.exit(1)

SERVER = sys.argv[1]
PORT = int(sys.argv[2])
REGISTER = sys.argv[4]
EXPIRES = sys.argv[5]

REQUEST = 'REGISTER sip: ' + str(REGISTER) + ' SIP/2.0 ' + EXPIRES

# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.connect((SERVER, PORT))

    print("Enviando:", REQUEST)
    my_socket.send(bytes(REQUEST, 'utf-8') + b'\r\n')
    data = my_socket.recv(1024)
    print('Recibido -- ', data.decode('utf-8'))

print("Socket terminado.")
