#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import json
from time import gmtime, strftime, time


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """
    dictionary_IP = {}

    def register2json(self):
        """
        Write the dictionary to file
        """
        with open('registered.json', 'w') as file:
            json.dump(self.dictionary_IP, file)

    def json2registered(self):
        """
        Check if there is a file, if it exists, it will read it
        """
        try:
            with open('registered.json', 'r') as file:
                self.dictionary_IP = json.load(file)
        except:
            pass

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        self.wfile.write(b"SIP/2.0 200 OK")
        for line in self.rfile:
            request = line.decode('utf8')
            user = request.split()[2]
            expires = request.split()[4]
            second = int(time()) + int(expires)
            temp = strftime('%Y-%m-%d %H:%M:%S', gmtime(second + 3600))
            temp_now = strftime('%Y-%m-%d %H:%M:%S', gmtime(int(time())+3600))
            lista = []

            print("El cliente nos manda ", request)

            if expires == '0':
                del self.dictionary_IP[user]
                print('SIP/2.0 200 OK')

            if request.split()[0] == 'REGISTER' and expires != '0':
                self.dictionary_IP[user] = [self.client_address[0], temp]

            for name in self.dictionary_IP:
                if temp_now >= self.dictionary_IP[name][1]:
                    lista.append(name)
                    print(lista)
            for name in lista:
                del self.dictionary_IP[name]

            print(self.dictionary_IP)
            self.register2json()
            self.json2registered()
            # print("La ip es: " + str(self.client_address[0]) + " y el puerto es: " + str(self.client_address[1]))

if __name__ == "__main__":
    # Listens at localhost ('') port 6001
    # and calls the EchoHandler class to manage the request
    PORT = int(sys.argv[1])
    serv = socketserver.UDPServer(('', PORT), SIPRegisterHandler)

    print("Lanzando servidor UDP de eco...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")

